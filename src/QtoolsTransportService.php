<?php

namespace Drupal\qtools_transport;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\qtools_common\Utility\ResultMetadata;
use Drupal\qtools_transport\Utility\QtoolsTransportCacheableObject;
use Drupal\qtools_transport\Utility\QtoolsTransportRequest;
use Drupal\qtools_common\QtoolsCacheHelper;
use Drupal\qtools_common\Utility\QtoolsConfigurableBase;
use Drupal\qtools_transport\Utility\QtoolsTransportResponse;
use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class TransportService.
 */
class QtoolsTransportService extends QtoolsConfigurableBase {

  use StringTranslationTrait;
  use LoggerChannelTrait;

  /**
   * GuzzleHttp\ClientInterface.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Drupal\Core\Http\ClientFactory.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $httpClientFactory;

  /**
   * Symfony\Component\HttpFoundation\RequestStack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Unique value for request.
   *
   * @var string
   */
  protected $traceKeyUniquieId;

  // Log levels.
  const LOG_NONE = 0;
  const LOG_ERROR = 1;
  const LOG_ALL = 2;

  // Successful code.
  const HTTP_CODE_OK = [200, 201, 204, 206];
  const HTTP_CODE_UNAUTHORIZED = 401;

  // Support extension.
  const CONTENT_TYPE_JSON = 'application/json';

  const STATUS_CODE_VALIDATION_FAILED = -1;
  const STATUS_CODE_INVALID_URL = -2;
  const STATUS_CODE_EXCEPTION = -3;

  const ERROR_KEY_MISSING_ARGUMENT = 'qtools_transport.missing_argument';
  const ERROR_KEY_PARSER_ERROR = 'qtools_transport.parser_error';
  const ERROR_KEY_EXCEPTION = 'qtools_transport.exception';
  const ERROR_KEY_INVALID_URL = 'qtools_transport.invalid_url';
  const ERROR_KEY_INVALID_STATUS_CODE = 'qtools_transport.invalid_status_code';

  const TRANSPORT_CACHE_BIN = 'qtools_transport_cache';
  const TRANSPORT_CACHE_TAG = 'qtools_transport_cache';

  const METHODS_CACHEABLE = ['GET'];

  const DISABLE_CACHE_REFRESH = -1;
  const DISABLE_CACHE_PERSISTENT = 1;
  const DISABLE_CACHE_ALL = 2;

  const CONFIG_DISABLE_SSL_VERIFICATION = 'CONFIG_DISABLE_SSL_VERIFICATION';
  const CONFIG_KEEP_RESPONSE_DATA = 'CONFIG_KEEP_RESPONSE_DATA';
  const CONFIG_TRANSPORT_CACHE_RULES = 'CONFIG_TRANSPORT_CACHE_RULES';

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ClientFactory $httpClientFactory,
    RequestStack $requestStack,
    UuidInterface $uuid
  ) {
    // Save references to other services.
    $this->httpClientFactory = $httpClientFactory;
    $this->requestStack = $requestStack;

    // Since service is booted up once a request,
    // we build unique id in constructor.
    $this->traceKeyUniquieId = $uuid->generate();
  }

  /**
   * Returns or create new HTTP client.
   */
  public function getHttpClient(): ClientInterface {
    if (empty($this->httpClient)) {
      $this->httpClient = $this->httpClientFactory->fromOptions($this->getInitialHttpClientOptions());
    }
    return $this->httpClient;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigTraitBundle(): string {
    return static::class;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigTraitDefaults(): array {
    return [
      static::CONFIG_TRANSPORT_CACHE_RULES => [],
      static::CONFIG_KEEP_RESPONSE_DATA => FALSE,
      static::CONFIG_DISABLE_SSL_VERIFICATION => TRUE,
    ];
  }

  /**
   * Creates new cacheable object, prepopulating its cacheable metadata.
   */
  public function newCacheableObject($class, $id, $properties): ?QtoolsTransportCacheableObject {
    if (!class_exists($class) || !is_subclass_of($class, QtoolsTransportCacheableObject::class)) {
      return NULL;
    }

    /** @var \Drupal\qtools_transport\Utility\QtoolsTransportCacheableObject $object */
    $object = new $class($id, $properties);
    $object->addCacheableDependency($this);
    return $object;
  }

  /**
   * Returns Initial HTTP client options.
   */
  public function getInitialHttpClientOptions(): array {
    $httpClientOptions = [
      'http_errors' => FALSE,
      'stream' => FALSE,
    ];
    if ($this->config(static::CONFIG_DISABLE_SSL_VERIFICATION)) {
      $httpClientOptions['verify'] = FALSE;
    }
    return $httpClientOptions;
  }

  /**
   * Returns default request headers.
   */
  public function getDefaultRequestHeaders(): array {
    return [
      'Content-Type' => 'application/json',
      'Accept' => static::CONTENT_TYPE_JSON . ',application/xml,application/zip,application/octet-stream',
    ];
  }

  /**
   * Builds error response suitable to be injected in request.
   */
  public function buildErrorResponse(int $code, string $message, string $key, mixed $info = NULL): array {
    return [
      'code' => $code,
      'data' => [
        'key' => $key,
        'message' => $message,
        'info' => $info,
      ],
    ];
  }

  /**
   * Builds successful response.
   */
  public function buildMockResponse(int $code, string $content, array $headers = []): array {
    return [
      'code' => $code,
      'content' => $content,
      'headers' => $headers,
    ];
  }

  /**
   * Create new request data object.
   */
  public function newTransportRequest(string $base_url, string $service_path, ?array $service_args = []): QtoolsTransportRequest {
    $request_data = new QtoolsTransportRequest();
    $request_data->setEndpoint($base_url, $service_path, $service_args);
    $request_data->setRequestHeaders($this->getDefaultRequestHeaders());
    return $request_data;
  }

  /**
   * Initiate network request.
   */
  public function executeTransportRequest(QtoolsTransportRequest $transportRequest): QtoolsTransportResponse {
    // Execute request, caching what is possible.
    if ($this->requestCanBeCached($transportRequest)) {
      $transportResponse = $this->executeCached($transportRequest);
    }
    else {
      // Requests other than get are not cached.
      $transportResponse = $this->executeDirect($transportRequest);
      $transportResponse->setMeta('cached_op', QtoolsCacheHelper::CACHE_FOUND_NONE);
    }

    return $transportResponse;
  }

  /**
   * Returns trace header content to trace request on remote.
   */
  protected function getTraceHeaderContent(): string {
    return implode(';', [
      $this->traceKeyUniquieId,
      $this->requestStack->getCurrentRequest()->getRequestUri(),
    ]);
  }

  /**
   * Check if request can be cached.
   */
  protected function requestCanBeCached(QtoolsTransportRequest $transportRequest): bool {
    // Request can't be cached if it is set as not cacheable,
    // or uses not cacheable method (like POST).
    return ($transportRequest->getDisabledCacheMode() != static::DISABLE_CACHE_ALL) &&
      in_array($transportRequest->toRequestParams()['method'], static::METHODS_CACHEABLE);
  }

  /**
   * Gets service key name.
   */
  public function serviceKey(QtoolsTransportRequest $transportRequest): string {
    return $transportRequest->getEndpoint()['path'];
  }

  /**
   * Returns service cache tag.
   */
  public static function serviceCacheTag($service_key = NULL): string {
    return static::TRANSPORT_CACHE_TAG . ':' . $service_key;
  }

  /**
   * Execute request adding caching layer on top.
   */
  protected function executeCached(QtoolsTransportRequest $transportRequest): QtoolsTransportResponse {
    // Allow all request to be potentially cached.
    [$service_cache_key, $ttl, $allowed_errors, $rule] = $this->getCacheSettings($transportRequest);

    // Cache keys.
    $cid_keys = $this->getCachingKeys($transportRequest);

    // If cache was set to refresh we drop any existing one for same keys.
    if ($transportRequest->getDisabledCacheMode() == static::DISABLE_CACHE_REFRESH) {
      QtoolsCacheHelper::cachedCallClear($cid_keys, static::class, $service_cache_key, static::TRANSPORT_CACHE_BIN);
    }

    // Call execute method wrapped into cached call.
    /** @var \Drupal\Core\Cache\CacheableMetadata $cachedCallCacheableMetadata */
    /** @var \Drupal\qtools_transport\Utility\QtoolsTransportResponse $transportResponse */
    [$transportResponse, $op, $cachedCallCacheableMetadata] = QtoolsCacheHelper::cachedCall([
      function (QtoolsTransportRequest $innerTransportRequest, CacheableMetadata $innerCacheableMetadata) use ($ttl, $allowed_errors) {
        // Launch real request.
        $innerTransportResponse = $this->executeDirect($innerTransportRequest);

        // If request end up with error skip long time caching,
        // but prevent re-query (unless allowed).
        if ($innerTransportResponse->hasErrors()) {
          // Check if this error is allowed.
          $keys = $innerTransportResponse->getErrorsKeys();
          $real_errors = array_diff($keys, $allowed_errors);
          $max_age = !empty($real_errors) ? QtoolsCacheHelper::TTL_STATIC : $ttl;
          $innerTransportResponse->setCacheMaxAge($max_age);
        }

        // Copy response metadata into cached call metadata.
        $innerCacheableMetadata->addCacheableDependency($innerTransportResponse);

        // Add service cache key to metadata.
        $innerCacheableMetadata->addCacheTags([
          static::TRANSPORT_CACHE_TAG,
          static::serviceCacheTag($this->serviceKey($innerTransportRequest)),
        ]);

        return $innerTransportResponse;
      },
      get_called_class() . ':execute',
    ], [$transportRequest], $cid_keys, $ttl, static::class, $service_cache_key, static::TRANSPORT_CACHE_BIN);

    $transportResponse->setMeta('cache_info', [
      'service_cache_key' => $service_cache_key,
      'cid_keys' => $cid_keys,
      'rule' => $rule,
      'ttl' => $ttl,
      'allowed_errors' => $allowed_errors,
      'metadata' => [
        'context' => $cachedCallCacheableMetadata->getCacheContexts(),
        'tags' => $cachedCallCacheableMetadata->getCacheTags(),
        'max_age' => $cachedCallCacheableMetadata->getCacheMaxAge(),
      ],
    ]);
    $transportResponse->setMeta('cached_op', $op);

    // Merge response metadata.
    $transportResponse->addCacheableDependency($cachedCallCacheableMetadata);

    return $transportResponse;
  }

  /**
   * Execute request according to specified params.
   */
  protected function executeDirect(QtoolsTransportRequest $transportRequest): QtoolsTransportResponse {
    // Clone request as it may be modified in the future,
    // and we don't want to silently modify original request.
    $transportResponse = new QtoolsTransportResponse(clone $transportRequest);

    // Pass response as ResultMetadata.
    $request_params = $transportRequest->toRequestParams($transportResponse);

    // If error - there is no need to even send this request,
    // so we generate response automatically.
    if ($transportResponse->hasErrors()) {
      $last_error = $transportResponse->getLastError();
      $error_response = $this->buildErrorResponse(
        static::STATUS_CODE_VALIDATION_FAILED,
        $last_error[0],
        $last_error[1],
      );
      $transportRequest->setResponseData($error_response);
    }

    // Add this service dependencies.
    QtoolsCacheHelper::extendMetadata($transportResponse, $this);

    // Unset Parser callable as it's a type of closure,
    // and can't be serialized.
    if ($transportRequest->isParserCallable()) {
      $parser_callable = $transportRequest->getParser();

      // Remove parser content from response.
      $transportResponse->getTransportRequest()->setParser(TRUE);
    }

    // Options shortcut.
    $options = $request_params['http_client_options'];

    // Prepare retry variables.
    $try_count = $request_params['try_count'] ?? 1;
    $attempts = 0;
    $timeout = $options['timeout'] ?? 10;
    $started = microtime(TRUE);

    // To store network exceptions.
    $exceptions = [];

    // Attempt to do request.
    do {
      // Update try count.
      $attempts++;
      $retry = FALSE;

      // Clean up results.
      $response_content = NULL;
      $response = NULL;
      $request_time = 0;
      $request_time_start = microtime(TRUE);
      $http_client_request_url = $request_params['base_url'] . '/' . ltrim($request_params['path'], '/');

      try {
        // Check if we already have a response.
        if (!empty($request_params['response'])) {
          $response_status_code = $request_params['response']['code'];
          $response_phrase = $request_params['response']['code'] ?? '';
          $response_content = $request_params['response']['content'];
          $response_headers = $request_params['response']['headers'] ?? [];
        }
        elseif (!filter_var($http_client_request_url, FILTER_VALIDATE_URL)) {
          // Generate error if endpoint is not set or invalid.
          $response_status_code = static::STATUS_CODE_INVALID_URL;
          $response_phrase = $this->t('Invalid endpoint url')->render();
          $response_content = '';
          $response_headers = [];
          $transportResponse->addError($response_phrase, static::ERROR_KEY_INVALID_URL);
        }
        else {
          // Issue external request.
          $response = $this->getHttpClient()->request(
            $request_params['method'],
            $http_client_request_url,
            $options,
          );

          $response_status_code = $response->getStatusCode();
          $response_phrase = $response->getReasonPhrase();
          $response_content = $this->parseResponseContent($response, $transportRequest);
          $response_headers = [];
          foreach ($response->getHeaders() as $header_name => $header_values) {
            $response_headers[$header_name] = implode('; ', $header_values);
          }

          // Stop request timer.
          $request_time = microtime(TRUE) - $request_time_start;
        }

        // Check for errors from response, we do it in separate method for
        // easier extension from other classes.
        $this->checkResponseErrors(
          $response_status_code,
          $response_phrase,
          $response_content,
          $response_headers,
          $request_params,
          $transportResponse,
        );
      }
      catch (\Exception $e) {
        $exceptions[] = [
          'code' => $e->getCode(),
          'message' => $e->getMessage(),
        ];

        // Generate error if endpoint is not set or invalid.
        $response_status_code = static::STATUS_CODE_EXCEPTION;
        $response_phrase = $e->getCode() . ' ' . $e->getMessage();
        $response_content = '';
        $response_headers = [];

        // Generate error.
        $transportResponse->addError($response_phrase, static::ERROR_KEY_EXCEPTION);

        // Attempt to retry if we within 1.5 of timeout window
        // (i.e. timed out services has only one retry).
        $retry = (microtime(TRUE) - $started) < $timeout * 1.5;
      }
    } while ($retry && ($attempts < $try_count));

    // Saving response values.
    $transportResponse->setResponseStatusCode($response_status_code);
    $transportResponse->setResponsePhrase($response_phrase);
    $transportResponse->setResponseHeaders($response_headers);
    $transportResponse->setResponseContent($response_content);
    $transportResponse->setExceptions($exceptions);

    // Apply parser to data object.
    if (!$transportResponse->hasErrors() && !empty($parser_callable)) {
      $parse_time_start = microtime(TRUE);
      $parsed = $parser_callable($transportResponse);
      $transportResponse->setParsed($parsed);
      $parse_time = microtime(TRUE) - $parse_time_start;

      // If parser returns NULL we got an unknown parser error.
      if ($parsed === NULL) {
        $transportResponse->addError($this->t('Parser error occurred'), static::ERROR_KEY_PARSER_ERROR);
      }
    }

    // For debug purposes we may want to keep original response data,
    // but otherwise we clear it to save memory.
    if (!empty($parser_callable) &&
      empty($this->config(static::CONFIG_KEEP_RESPONSE_DATA)) &&
      !$transportRequest->getKeepResponseContentFlag()
    ) {
      $transportResponse->setResponseContent(TRUE);
    }

    // Save performance counters.
    $transportResponse->setMeta('performance', [
      'request_time' => $request_time,
      'parse_time' => $parse_time ?? 0,
    ]);

    // Request is not cacheable if it failed.
    if ($transportResponse->hasErrors()) {
      $transportResponse->mergeCacheMaxAge(QtoolsCacheHelper::MAX_AGE_NO_CACHE);
    }

    return $transportResponse;
  }

  /**
   * Parses response body/content.
   */
  protected function parseResponseContent(ResponseInterface $response, QtoolsTransportRequest $transportRequest): mixed {
    return $response->getBody()->getContents();
  }

  /**
   * Extract errors from response.
   */
  protected function checkResponseErrors(int $response_code, string $response_phrase, string $response_content, array $response_headers, array $request_params, ResultMetadata $resultMetadata): void {
    // Ignore negative response codes as they correspond to internal errors,
    // for those errors array already filled and no request was sent.
    if ($response_code > 0 && !in_array($response_code, self::HTTP_CODE_OK)) {
      $resultMetadata->addError($response_code . ' ' . $response_phrase, static::ERROR_KEY_INVALID_STATUS_CODE);
    }
  }

  /**
   * Get cache keys (cid).
   */
  protected function getCachingKeys(QtoolsTransportRequest $transportRequest): array {
    $request_params = $transportRequest->toRequestParams();
    $keys[] = $request_params['base_url'];
    $keys[] = $request_params['path'];
    $keys[] = $request_params['http_client_options']['query'];

    // Support cache_keys passed from outside.
    return array_merge($keys, $request_params['cache_keys']);
  }

  /**
   * Get caching ttl for request based on its service.
   */
  protected function getCacheSettings(QtoolsTransportRequest $transportRequest): array {
    $disable_cache_mode = $transportRequest->getDisabledCacheMode();
    $service_key = $this->serviceKey($transportRequest);
    if (empty($disable_cache_mode) || $disable_cache_mode <= 0) {
      $rules = &drupal_static(static::class . '_transport_cache_rules', NULL);
      if ($rules === NULL) {
        $rules = $this->config(static::CONFIG_TRANSPORT_CACHE_RULES);
      }
      foreach ($rules as $line) {
        $line = trim($line);
        if (!empty($line) && !str_contains($line, '#') && str_starts_with($line, $service_key . '|')) {
          $rule = explode('|', $line);
          $allowed_errors = !empty($rule[2]) ? explode(',', $rule[2]) : [];
          return [$service_key, (int) $rule[1], $allowed_errors, $line];
        }
      }
    }
    return [$service_key, QtoolsCacheHelper::TTL_STATIC, [], NULL];
  }

}
