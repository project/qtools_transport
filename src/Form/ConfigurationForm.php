<?php

namespace Drupal\qtools_transport\Form;

use Drupal\qtools_common\Utility\QtoolsConfigFormBase;
use Drupal\qtools_transport\QtoolsTransportService;

/**
 * Configure Qtools Transport for this site.
 */
class ConfigurationForm extends QtoolsConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return self::class;
  }

  /**
   * {@inheritdoc}
   */
  public function getSections(): array {
    $sections['caching'] = [
      'title' => $this->t("Caching configuration"),
      'description' => $this->t('Caching rules can be specified as ....'),
      'form_elements' => [
        QtoolsTransportService::class . "][" . QtoolsTransportService::CONFIG_TRANSPORT_CACHE_RULES => $this->t('List of cache rules, one per line @example', ['@example' => '<service_key>|<ttl_seconds_success>|<allowed_errors>|<ttl_seconds_error>']),
      ],
    ];

    return $sections;
  }

  /**
   * {@inheritdoc}
   */
  public function trackingEnabled(): bool {
    return TRUE;
  }

}
