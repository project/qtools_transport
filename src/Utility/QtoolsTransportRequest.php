<?php

namespace Drupal\qtools_transport\Utility;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\qtools_common\Utility\ResultMetadata;

/**
 * Transport Request class.
 */
class QtoolsTransportRequest {

  use StringTranslationTrait;

  /**
   * Properties bag.
   *
   * @var array
   */
  protected $data = [];

  /**
   * Cached version of params.
   *
   * @var array
   */
  protected $cachedParams;

  /**
   * Cached version of curl command.
   *
   * @var string
   */
  protected $cachedCurlCommand;

  const DEFAULT_METHOD = 'GET';

  const DATA_KEY_BODY_ARRAY = 'body_array';
  const DATA_KEY_BODY_CONTENT = 'body_content';
  const DATA_KEY_KEEP_RESPONSE_CONTENT = 'keep_response_content';
  const DATA_KEY_FORM_PARAMS = 'form_params';
  const DATA_KEY_HEADERS = 'headers';
  const DATA_KEY_PARSER = 'parser';
  const DATA_KEY_QUERY_ARRAY = 'query';
  const DATA_KEY_ENDPOINT = 'endpoint';
  const DATA_KEY_RESPONSE_DATA = 'response_data';
  const DATA_KEY_CACHE_KEYS = 'cache_keys';
  const DATA_KEY_DISABLE_CACHE_MODE = 'disable_cache';
  const DATA_KEY_TIMEOUT = 'timeout';
  const DATA_KEY_PROXY = 'proxy';
  const DATA_KEY_TRY_COUNT = 'try_count';
  const DATA_KEY_FILE_UPLOAD = 'file';
  const DATA_KEY_HTTP_CLIENT_OPTIONS = 'http_client_options';

  /**
   * Sets data property and clear cached values.
   */
  protected function setDataProperty(string $key, mixed $value = NULL): static {
    $this->data[$key] = $value;

    // Clear cached values.
    $this->cachedCurlCommand = NULL;
    $this->cachedParams = NULL;
    return $this;
  }

  /**
   * Sets HttpClientOptions.
   */
  public function setHttpClientOptions(array $httpClientOptions = [], $merge = TRUE): static {
    if ($merge) {
      $old_options = $this->getHttpClientOptions();
      $httpClientOptions = $httpClientOptions + $old_options;
    }
    return $this->setDataProperty(static::DATA_KEY_HTTP_CLIENT_OPTIONS, $httpClientOptions);
  }

  /**
   * Gets HttpClientOptions.
   */
  public function getHttpClientOptions(): array {
    $options = $this->data[static::DATA_KEY_HTTP_CLIENT_OPTIONS] ?? [];

    // Move headers to options.
    if ($this->isRequestHeadersSet()) {
      $options['headers'] = $this->getRequestHeaders();
    }

    // Move body into options.
    if ($this->isBodySet()) {
      $options['body'] = $this->getBody();
    }

    // Move form params into options.
    if ($this->isFormParamsSet()) {
      $options['headers']['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';
      $options['form_params'] = $this->getFormParams();
    }

    // Move timeout to options.
    if ($this->isTimeoutSet()) {
      $options['timeout'] = $this->getTimeout();
    }

    // Add custom proxy if specified.
    if ($this->isProxySet()) {
      $options['proxy'] = $this->getProxy();
    }

    if ($this->isFileUploadSet()) {
      $options['headers']['Content-Type'] = 'multipart/form-data';

      // Compile headers for direct inclusion.
      $curl_headers = [];
      foreach ($options['headers'] as $name => $value) {
        $curl_headers[] = $name . ': ' . $value;
      }

      // Create file upload payload.
      $file_info = $this->getFileUpload();
      $curl_post_fields = [
        'file' => curl_file_create($file_info[0], $file_info[1]),
      ];
      $curl_options = [
        CURLOPT_HTTPHEADER => $curl_headers,
        CURLOPT_POSTFIELDS => $curl_post_fields,
      ];

      // Overwrite guzzle cURL options.
      $options['curl'] = $curl_options;
    }

    // Move all normal params into query.
    $options['query'] = $this->getQueryArray();

    return $options;
  }

  /**
   * Sets RequestHeaders.
   */
  public function setRequestHeaders(array $requestHeaders = [], $merge = TRUE): static {
    if ($merge) {
      $old_headers = $this->getRequestHeaders();
      $requestHeaders = $requestHeaders + $old_headers;
    }
    return $this->setDataProperty(static::DATA_KEY_HEADERS, $requestHeaders);
  }

  /**
   * Gets RequestHeaders.
   */
  public function getRequestHeaders(): array {
    return $this->data[static::DATA_KEY_HEADERS] ?? [];
  }

  /**
   * Checks if RequestHeaders are set.
   */
  public function isRequestHeadersSet(): bool {
    return isset($this->data[static::DATA_KEY_HEADERS]);
  }

  /**
   * Sets Endpoint/service.
   */
  public function setEndpoint(string $base_url, string $path, ?array $args = []): static {
    return $this->setDataProperty(static::DATA_KEY_ENDPOINT, [
      'base_url' => $base_url,
      'path' => $path,
      'args' => $args,
    ]);
  }

  /**
   * Set endpoint args, do nothing if endpoint is not yet set.
   */
  public function addEndpointArgs($args = []): static {
    $endpoint = $this->getEndpoint();
    if (empty($endpoint)) {
      return $this;
    }

    // Merge args prioritizing new ones.
    $endpoint['args'] = $args + $endpoint['args'];
    return $this->setDataProperty(static::DATA_KEY_ENDPOINT, $endpoint);
  }

  /**
   * Gets Endpoint/service.
   */
  public function getEndpoint(): ?array {
    return $this->data[static::DATA_KEY_ENDPOINT] ?? NULL;
  }

  /**
   * Sets Parser.
   */
  public function setParser(mixed $parser_function): static {
    return $this->setDataProperty(static::DATA_KEY_PARSER, $parser_function);
  }

  /**
   * Gets Parser.
   */
  public function getParser(): mixed {
    return $this->data[static::DATA_KEY_PARSER] ?? NULL;
  }

  /**
   * Checks if Parser is set and is callable.
   */
  public function isParserCallable(): bool {
    return !empty($this->data[static::DATA_KEY_PARSER]) && is_callable($this->data[static::DATA_KEY_PARSER]);
  }

  /**
   * Sets QueryArray.
   */
  public function setQueryArray($query_array): static {
    return $this->setDataProperty(static::DATA_KEY_QUERY_ARRAY, $query_array);
  }

  /**
   * Gets QueryArray.
   */
  public function getQueryArray(): array {
    return $this->data[static::DATA_KEY_QUERY_ARRAY] ?? [];
  }

  /**
   * Sets CacheKeys.
   */
  public function setCacheKeys($cache_keys): static {
    return $this->setDataProperty(static::DATA_KEY_CACHE_KEYS, $cache_keys);
  }

  /**
   * Gets CacheKeys.
   */
  public function getCacheKeys(): array {
    return $this->data[static::DATA_KEY_CACHE_KEYS] ?? [];
  }

  /**
   * Sets ResponseData.
   */
  public function setResponseData(array $response): static {
    return $this->setDataProperty(static::DATA_KEY_RESPONSE_DATA, $response);
  }

  /**
   * Gets ResponseData.
   */
  public function getResponseData(): ?array {
    return $this->data[static::DATA_KEY_RESPONSE_DATA] ?? NULL;
  }

  /**
   * Sets DisabledCacheMode.
   */
  public function setDisabledCacheMode(int $disabled_cache_mode): static {
    return $this->setDataProperty(static::DATA_KEY_DISABLE_CACHE_MODE, $disabled_cache_mode);
  }

  /**
   * Gets DisabledCacheMode.
   */
  public function getDisabledCacheMode(): ?int {
    return $this->data[static::DATA_KEY_DISABLE_CACHE_MODE] ?? NULL;
  }

  /**
   * Sets TryCount.
   */
  public function setTryCount(int $retry_count): static {
    return $this->setDataProperty(static::DATA_KEY_TRY_COUNT, $retry_count);
  }

  /**
   * Gets TryCount.
   */
  public function getTryCount(): int {
    return $this->data[static::DATA_KEY_TRY_COUNT] ?? 1;
  }

  /**
   * Sets Timeout.
   */
  public function setTimeout(int $timeout): static {
    return $this->setDataProperty(static::DATA_KEY_TIMEOUT, $timeout);
  }

  /**
   * Gets Timeout.
   */
  public function getTimeout(): ?int {
    return $this->data[static::DATA_KEY_TIMEOUT] ?? NULL;
  }

  /**
   * Checks if Timeout is set.
   */
  public function isTimeoutSet(): bool {
    return isset($this->data[static::DATA_KEY_TIMEOUT]);
  }

  /**
   * Sets Proxy.
   */
  public function setProxy(string $proxy): static {
    return $this->setDataProperty(static::DATA_KEY_PROXY, $proxy);
  }

  /**
   * Gets Proxy.
   */
  public function getProxy(): ?string {
    return $this->data[static::DATA_KEY_PROXY] ?? NULL;
  }

  /**
   * Checks if Proxy is set.
   */
  public function isProxySet(): bool {
    return isset($this->data[static::DATA_KEY_PROXY]);
  }

  /**
   * Sets KeepResponseContentFlag.
   */
  public function setKeepResponseContentFlag(bool $value): static {
    return $this->setDataProperty(static::DATA_KEY_KEEP_RESPONSE_CONTENT, $value);
  }

  /**
   * Gets KeepResponseContentFlag.
   */
  public function getKeepResponseContentFlag(): bool {
    return !empty($this->data[static::DATA_KEY_KEEP_RESPONSE_CONTENT]);
  }

  /**
   * Sets FileUpload.
   */
  public function setFileUpload(mixed $file_upload): static {
    return $this->setDataProperty(static::DATA_KEY_FILE_UPLOAD, $file_upload);
  }

  /**
   * Gets FileUpload.
   */
  public function getFileUpload(): mixed {
    return $this->data[static::DATA_KEY_FILE_UPLOAD] ?? NULL;
  }

  /**
   * Checks if FileUpload is set.
   */
  public function isFileUploadSet(): bool {
    return isset($this->data[static::DATA_KEY_FILE_UPLOAD]);
  }

  /**
   * Sets FormParams.
   */
  public function setFormParams(array $form_params): static {
    return $this->setDataProperty(static::DATA_KEY_FORM_PARAMS, $form_params);
  }

  /**
   * Gets FormParams.
   */
  public function getFormParams(): ?array {
    return $this->data[static::DATA_KEY_FORM_PARAMS] ?? NULL;
  }

  /**
   * Checks if FormParams are set.
   */
  public function isFormParamsSet(): bool {
    return isset($this->data[static::DATA_KEY_FORM_PARAMS]);
  }

  /**
   * Sets BodyArray.
   */
  public function setBodyArray(array $body_array): static {
    return $this->setDataProperty(static::DATA_KEY_BODY_ARRAY, $body_array)
      ->setDataProperty(static::DATA_KEY_BODY_CONTENT, NULL);
  }

  /**
   * Gets BodyArray.
   */
  public function getBodyArray(): ?array {
    return $this->data[static::DATA_KEY_BODY_ARRAY];
  }

  /**
   * Sets BodyContent.
   */
  public function setBodyContent(string $body_content): static {
    return $this->setDataProperty(static::DATA_KEY_BODY_ARRAY, NULL)
      ->setDataProperty(static::DATA_KEY_BODY_CONTENT, $body_content);
  }

  /**
   * Gets BodyContent.
   */
  public function getBodyContent(): ?string {
    return $this->data[static::DATA_KEY_BODY_CONTENT];
  }

  /**
   * Checks if Body is set.
   */
  public function isBodySet(): bool {
    return isset($this->data[static::DATA_KEY_BODY_ARRAY]) ||
      isset($this->data[static::DATA_KEY_BODY_CONTENT]);
  }

  /**
   * Returns Json encoded body or plain content.
   */
  public function getBody(): bool|string|NULL {
    $body_array = $this->getBodyArray();
    if ($body_array !== NULL) {
      return Json::encode($body_array);
    }
    return $this->getBodyContent();
  }

  /**
   * Returns whole data property.
   */
  public function getData(): array {
    return $this->data;
  }

  /**
   * Returns endpoint path string.
   */
  public function buildEndpointPath(?ResultMetadata $resultMetadata = NULL): ?string {
    $endpoint = $this->getEndpoint();
    if (empty($endpoint['base_url'])) {
      $resultMetadata?->addError($this->t("Can't build endpoint path without [ base_url ]."));
      return NULL;
    }

    $path = $endpoint['path'];
    $args = $endpoint['args'];

    if (empty($path)) {
      $resultMetadata?->addError($this->t("Can't build endpoint path without [ path ]."));
      return NULL;
    }

    if (!empty($args)) {
      $replacement = [];

      foreach ($args as $key => $value) {
        // Empty values are not supported and thus errors.
        if (empty($value)) {
          $resultMetadata?->addError($this->t("Can't build endpoint path, placeholder [ @key ] value cant be empty.", [
            '@key' => $key,
          ]));
          return NULL;
        }
        else {
          $replacement[$key] = $value;
        }
      }
      $path = strtr($path, $replacement);
    }

    // If path still contains placeholders we fire an error.
    if (str_contains($path, '{')) {
      $resultMetadata?->addError($this->t("Can't build endpoint path, placeholder [ {id?} ] does not have value."));
      return NULL;
    }

    return $path;
  }

  /**
   * Converts transport request into request params.
   */
  public function toRequestParams(?ResultMetadata $resultMetadata = NULL): array {
    $resultMetadata = $resultMetadata ?? new ResultMetadata();

    // Don't build params if we have them in cache.
    if (!empty($this->cachedParams)) {
      return $this->cachedParams;
    }

    // Get Endpoint.
    $endpoint = $this->getEndpoint();

    $request_params['endpoint'] = $endpoint;

    // Get parser if present.
    $request_params['parser'] = $this->getParser();

    // Get response if present.
    $request_params['response'] = $this->getResponseData();

    // Move cache keys into options.
    $request_params['cache_keys'] = $this->getCacheKeys();

    // Move cache keys into options.
    $request_params['disable_cache'] = $this->getDisabledCacheMode();

    // Build string endpoint path.
    $endpoint_path = $this->buildEndpointPath($resultMetadata);

    // Request method and path.
    if (str_contains($endpoint_path, ':')) {
      [$method, $endpoint_path] = explode(':', $endpoint_path);
    }
    else {
      $method = static::DEFAULT_METHOD;
    }

    $request_params['method'] = $method;
    $request_params['path'] = $endpoint_path;
    $request_params['base_url'] = $endpoint['base_url'];
    $request_params['try_count'] = $this->getTryCount();

    // Rest of the http client options.
    $request_params['http_client_options'] = $this->getHttpClientOptions();

    // If no errors save this requestParams into cache.
    if (!$resultMetadata->hasErrors()) {
      $this->cachedParams = $request_params;
    }

    return $request_params;
  }

  /**
   * Loosely converts request to curl command.
   */
  public function toCurlCommand(?ResultMetadata $resultMetadata = NULL): ?string {
    $resultMetadata = $resultMetadata ?? new ResultMetadata();

    // Don't build new command if we have them in cache.
    if (!empty($this->cachedCurlCommand)) {
      return $this->cachedCurlCommand;
    }

    // Convert to params.
    $request_params = $this->toRequestParams($resultMetadata);

    // Don't build command if there are errors.
    if ($resultMetadata->hasErrors()) {
      return NULL;
    }

    // Options shortcut.
    $options = $request_params['http_client_options'];

    // Adding query params.
    if (!empty($options['query'])) {
      $query = UrlHelper::buildQuery($options['query']);
    }

    // Base command, includes verbose and cert ignore.
    $url = $request_params['base_url'] . '/' . $request_params['path'];
    if (!empty($query)) {
      $url .= '?' . $query;
    }

    // Method.
    if (strtoupper($request_params['method']) != 'GET') {
      $method = ' -X ' . strtoupper($request_params['method']);
    }
    else {
      $method = '';
    }

    $curl_command = 'curl -v -k ' . $method . ' "' . $url . '"';

    // Adding headers.
    if (!empty($options['headers'])) {
      foreach ($options['headers'] as $name => $value) {
        $curl_command .= ' -H "' . $name . ': ' . $value . '"';
      }
    }

    // Adding form params.
    if (!empty($options['form_params'])) {
      $curl_command .= ' -d "' . UrlHelper::buildQuery($options['form_params']);
    }

    // Save to cache if no errors.
    if (!$resultMetadata->hasErrors()) {
      $this->cachedCurlCommand = $curl_command;
    }

    return $curl_command;
  }

}
