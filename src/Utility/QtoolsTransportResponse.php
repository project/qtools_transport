<?php

namespace Drupal\qtools_transport\Utility;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\qtools_common\Utility\ResultMetadata;
use Psr\Log\LoggerInterface;

/**
 * Transport response class.
 */
class QtoolsTransportResponse extends ResultMetadata {

  /**
   * Properties bag.
   *
   * @var array
   */
  protected $data = [];

  const DATA_KEY_TRANSPORT_REQUEST = 'transport_request';
  const DATA_KEY_RESPONSE_STATUS_CODE = 'response_status_code';
  const DATA_KEY_RESPONSE_PHRASE = 'response_phrase';
  const DATA_KEY_RESPONSE_HEADERS = 'response_headers';
  const DATA_KEY_RESPONSE_CONTENT = 'response_content';
  const DATA_KEY_EXCEPTIONS = 'exceptions';
  const DATA_KEY_PARSED = 'parsed';
  const DATA_KEY_META = 'meta';

  const CONTENT_TYPE_JSON = 'application/json';
  const CONTENT_TYPE_XML = 'application/xml';
  const CONTENT_TYPE_HTML = 'text/html';

  const ERROR_KEY_PARSE_XML_INVALID_CONTENT = 'qtools_transport.response.parse_xml_invalid_content';
  const ERROR_KEY_PARSE_JSON_INVALID_CONTENT = 'qtools_transport.response.parse_json_invalid_content';

  /**
   * Constructor.
   */
  public function __construct(QtoolsTransportRequest $transportRequest, ?LoggerInterface $logger = NULL, ?MessengerInterface $messenger = NULL) {
    parent::__construct($logger, $messenger);
    $this->setTransportRequest($transportRequest);
    $this->setMeta('timestamp', time());
  }

  /**
   * Creates fake response structure for development.
   */
  public static function fakeResponse(string $content, array $headers_array = [], int $status_code = 200, string $phrase = 'OK'): array {
    return [
      'status_code' => $status_code,
      'phrase' => $phrase,
      'headers_array' => $headers_array,
      'content' => $content,
    ];
  }

  /**
   * Sets DataProperty and clears cached values (if any).
   */
  protected function setDataProperty(string $key, mixed $value = NULL): static {
    $this->data[$key] = $value;
    return $this;
  }

  /**
   * Sets TransportRequest.
   */
  public function setTransportRequest(QtoolsTransportRequest $value): static {
    return $this->setDataProperty(static::DATA_KEY_TRANSPORT_REQUEST, $value);
  }

  /**
   * Gets TransportRequest.
   */
  public function getTransportRequest(): ?QtoolsTransportRequest {
    return $this->data[static::DATA_KEY_TRANSPORT_REQUEST] ?? NULL;
  }

  /**
   * Sets ResponseStatusCode.
   */
  public function setResponseStatusCode(int $value): static {
    return $this->setDataProperty(static::DATA_KEY_RESPONSE_STATUS_CODE, $value);
  }

  /**
   * Gets ResponseStatusCode.
   */
  public function getResponseStatusCode(): ?int {
    return $this->data[static::DATA_KEY_RESPONSE_STATUS_CODE] ?? NULL;
  }

  /**
   * Sets ResponsePhrase.
   */
  public function setResponsePhrase(string $value): static {
    return $this->setDataProperty(static::DATA_KEY_RESPONSE_PHRASE, $value);
  }

  /**
   * Gets ResponsePhrase.
   */
  public function getResponsePhrase(): ?string {
    return $this->data[static::DATA_KEY_RESPONSE_PHRASE] ?? NULL;
  }

  /**
   * Sets ResponseHeaders.
   */
  public function setResponseHeaders(array $value): static {
    return $this->setDataProperty(static::DATA_KEY_RESPONSE_HEADERS, $value);
  }

  /**
   * Gets ResponseHeaders.
   */
  public function getResponseHeaders(): ?array {
    return $this->data[static::DATA_KEY_RESPONSE_HEADERS] ?? [];
  }

  /**
   * Sets ResponseContent.
   */
  public function setResponseContent(string|bool $value): static {
    return $this->setDataProperty(static::DATA_KEY_RESPONSE_CONTENT, $value);
  }

  /**
   * Gets ResponseContent.
   */
  public function getResponseContent(): string|bool|NULL {
    return $this->data[static::DATA_KEY_RESPONSE_CONTENT] ?? NULL;
  }

  /**
   * Checks if ResponseContent is ready for parsing.
   */
  public function isResponseContentReady(): bool {
    $content = $this->getResponseContent();
    return !empty($content) || is_string($content);
  }

  /**
   * Sets Exceptions.
   */
  public function setExceptions(array $value): static {
    return $this->setDataProperty(static::DATA_KEY_EXCEPTIONS, $value);
  }

  /**
   * Gets Exceptions.
   */
  public function getExceptions(): ?array {
    return $this->data[static::DATA_KEY_EXCEPTIONS] ?? NULL;
  }

  /**
   * Sets Parsed.
   */
  public function setParsed(mixed $value): static {
    return $this->setDataProperty(static::DATA_KEY_PARSED, $value);
  }

  /**
   * Gets Parsed.
   */
  public function getParsed(): mixed {
    return $this->data[static::DATA_KEY_PARSED] ?? NULL;
  }

  /**
   * Sets Meta (only partial partial).
   */
  public function setMeta(string $key, mixed $value): static {
    $meta_value = $this->getMetaAll();
    $meta_value[$key] = $value;
    return $this->setDataProperty(static::DATA_KEY_META, $meta_value);
  }

  /**
   * Gets Meta (whole or partial).
   */
  public function getMeta(string $key = NULL): mixed {
    return $this->data[static::DATA_KEY_META][$key] ?? NULL;
  }

  /**
   * Gets Meta (whole or partial).
   */
  public function getMetaAll(): array {
    return $this->data[static::DATA_KEY_META] ?? [];
  }

  /**
   * Parses/Converts response content from Json into array.
   */
  public function toArrayFromJson(?ResultMetadata $resultMetadata = NULL, ?bool $ignore_content_type = FALSE): ?array {
    if (!$this->isResponseContentReady()) {
      $resultMetadata?->addError($this->t("Response content is not ready (not set or already consumed)"));
      return NULL;
    }

    // Get response Content type.
    if (!$ignore_content_type) {
      $content_type = $this->getResponseHeaders()['Content-Type'] ?? '';
      if (!str_contains($content_type, static::CONTENT_TYPE_JSON)) {
        $resultMetadata?->addError($this->t("Can't parse response type [ @type ] as [ @expected ]", [
          '@type' => $content_type,
          '@expected' => static::CONTENT_TYPE_JSON,
        ]));
        return NULL;
      }
    }

    // Decode content as json.
    try {
      // Hide errors output as we are catching exception anyway.
      $content = $this->getResponseContent();
      $parsed = @Json::decode($content);
    }
    catch (\Exception $exception) {
      $parsed = NULL;
      $resultMetadata?->addError($this->t("Can't parse response, Json exception [ @code ] [ @message ]", [
        '@code' => $exception->getCode(),
        '@message' => $exception->getMessage(),
      ]), static::ERROR_KEY_PARSE_JSON_INVALID_CONTENT);
    }
    return $parsed;
  }

  /**
   * Parses/Converts response content from XML/HTML into SimpleXMLElement.
   */
  public function toSimpleXmlElementFromXml(?ResultMetadata $resultMetadata = NULL, ?bool $ignore_content_type = FALSE): ?\SimpleXMLElement {
    if (!$this->isResponseContentReady()) {
      $resultMetadata?->addError($this->t("Response content is not ready (not set or already consumed)"));
      return NULL;
    }

    // Get response Content type.
    if (!$ignore_content_type) {
      $content_type = $this->getResponseHeaders()['Content-Type'] ?? '';
      if (!str_contains($content_type, static::CONTENT_TYPE_XML) && !str_contains($content_type, static::CONTENT_TYPE_HTML)) {
        $resultMetadata?->addError($this->t("Can't parse response type [ @type ] as [ @expected ]", [
          '@type' => $content_type,
          '@expected' => static::CONTENT_TYPE_XML . ', ' . static::CONTENT_TYPE_HTML,
        ]));
        return NULL;
      }
    }

    // Decode content as XML.
    $parsed = NULL;
    try {
      // Hide errors output as we are catching exception anyway.
      $content = $this->getResponseContent();
      $parsed = @new \SimpleXMLElement($content);
    }
    catch (\Exception $exception) {
      $resultMetadata?->addError($this->t("Can't parse response, XML exception [ @code ] [ @message ]", [
        '@code' => $exception->getCode(),
        '@message' => $exception->getMessage(),
      ]), static::ERROR_KEY_PARSE_XML_INVALID_CONTENT);
    }
    return $parsed;
  }

  /**
   * Recursively convert simplexml element into php array.
   */
  public function simpleXmlElementToArray(\SimpleXMLElement $element): array {
    // Parser function, modified version of.
    // @see https://stackoverflow.com/a/50837043
    $parser = function (\SimpleXMLElement $xml) use (&$parser): array {
      $value = [];
      $nodes = $xml->children();
      $attributes = $xml->attributes();

      if (count($attributes) > 0) {
        foreach ($attributes as $attrName => $attrValue) {
          $value['@attributes'][$attrName] = strval($attrValue);
        }
      }

      if ($nodes->count() === 0) {
        $value['@value'] = strval($xml);
        return $value;
      }
      else {
        foreach ($nodes as $nodeName => $nodeValue) {
          $value[$nodeName][] = $parser($nodeValue);
        }
      }

      return $value;
    };

    return [
      $element->getName() => $parser($element),
    ];
  }

  /**
   * Creates new cacheable object, prepopulating its cacheable metadata.
   */
  public function newCacheableObject($class, $id, $properties): ?QtoolsTransportCacheableObject {
    if (!class_exists($class) || !is_subclass_of($class, QtoolsTransportCacheableObject::class)) {
      return NULL;
    }

    /** @var \Drupal\qtools_transport\Utility\QtoolsTransportCacheableObject $object */
    $object = new $class($id, $properties);
    $object->addCacheableDependency($this);
    return $object;
  }

}
