<?php

namespace Drupal\qtools_transport\Utility;

use Drupal\Core\Cache\CacheableMetadata;

/**
 * Cacheable object (dto).
 */
abstract class QtoolsTransportCacheableObject extends CacheableMetadata {

  /**
   * Object id.
   *
   * @var string
   */
  protected $id;

  /**
   * Names of custom properties.
   *
   * @var array
   */
  protected $propertiesList;

  // Analog of entity type id.
  const ENTITY_TYPE_ID = 'qtools_transport_cacheable_object';

  /**
   * Populate properties.
   */
  public function __construct(string $id, array $properties) {
    $this->id = $id;
    foreach ($properties as $key => $value) {
      $name = static::toCamelCase($key);
      $this->{$name} = $value;
      $this->propertiesList[$name] = $key;
    }
  }

  /**
   * Converts snake case to Camel/Pascal case.
   */
  public static function toCamelCase(string $string, bool $uc_first = FALSE): string {
    $str = str_replace('_', '', ucwords($string, '_'));
    $str = str_replace('-', '', ucwords($str, '-'));
    return $uc_first ? $str : lcfirst($str);
  }

  /**
   * Return this objects "entity type" analog.
   */
  public function getEntityTypeId(): string {
    return static::ENTITY_TYPE_ID;
  }

  /**
   * Return this objects "entity id" analog.
   */
  public function id() {
    return $this->id;
  }

  /**
   * Return this objects properties.
   */
  public function getProperties(bool $original_keys = FALSE) {
    foreach ($this->propertiesList as $property_key => $array_key) {
      $key = $original_keys ? $array_key : $property_key;
      $values[$key] = $this->{$property_key};
    }
    return $values;
  }

  /**
   * Must return this object "bundle/type".
   */
  abstract public function bundle(): string;

  /**
   * Exports object definition for easy properties population.
   */
  public static function exportDefinition(self $object): string {
    $template = '  /**' . PHP_EOL;
    $template .= '   * Property: @key.' . PHP_EOL;
    $template .= '   *' . PHP_EOL;
    $template .= '   * @var @type' . PHP_EOL;
    $template .= '   */' . PHP_EOL;
    $template .= '  public $@name;' . PHP_EOL;
    $template .= PHP_EOL;

    $output = '';

    $exclusion_keys = ['id', 'cacheContexts', 'cacheTags', 'cacheMaxAge', 'propertiesList'];

    foreach (get_object_vars($object) as $key => $value) {
      if (in_array($key, $exclusion_keys)) {
        continue;
      }
      $type = 'mixed';
      if (is_string($value)) {
        $type = 'string';
      }
      elseif (is_array($value)) {
        $type = 'array';
      }
      elseif (is_int($value)) {
        $type = 'int';
      }
      elseif (is_bool($value)) {
        $type = 'bool';
      }
      $replacements = [
        '@type' => $type,
        '@key' => $key,
        '@name' => static::toCamelCase($key),
      ];
      $output .= strtr($template, $replacements);
    }

    return $output;

  }

}
